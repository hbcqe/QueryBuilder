<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--显示适应手机-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<link href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
	<!-- bootstrap-date -->
<link href="<%=path%>/css/bootstrap-datetimepicker.css"
	rel="stylesheet" type="text/css">
<!-- 主样式 -->
<link href="<%=path%>/css/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=path%>/js/jquery-1.12.3.min.js"></script>

<script type="text/javascript"
	src="<%=path%>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<!-- bootstrap-date -->
<script type="text/javascript"
	src="<%=path%>/js/bootstrap-datetimepicker.js"></script>

<!-- bootstrap-typeahead -->
<script type="text/javascript"
	src="<%=path%>/js/Bootstrap-3-Typeahead.js"></script>

<!-- 全局参数文件 -->
<script type="text/javascript" src="<%=path%>/js/localPara.js"></script>

<!-- 自定义方法文件 -->
<script type="text/javascript"
	src="<%=path%>/js/queryBuilder.method.js"></script>

<!-- 主方法文件 -->
<script type="text/javascript"
	src="<%=path%>/js/jquery.queryBuilder.js"></script>
<script type="text/javascript">
	$(function() {
		$("#sql_builder").queryBuilder(
				{
					sourceData : sourceData1,//元数据
					saveJson : saveJson1,//保存的查询结构
					propertiesTypes : propertiesTypes1,//属性-类型-值 对应关系
					submitButton : "submit_btn",
					SQLButton : "SQL_btn",
					onPageClicked : function(entity, property, type) { //当属性和key-value连接符发生变化时，触发
					//				alert(entity+'.....'+property);
						if (type == "string") {
							return [ "郭鹏飞-$-1001", "李四-$-1005", "张山-$-1002",
									"王五-$-1003", "郭达-$-1003", "郭沫若-$-1003",
									"郭叔-$-1003", "郭子仪-$-1003" ];
						}

						return null;
					},
					onSQLBuild : function(SQLFabric, SQLJson) { //传入SQL结构和SQL语句

					}
				});
	});
</script>
</head>
<body>
	<ul class="nav nav-tabs" id="query_ul">
		<li role="presentation" id="query_template"><a
			href="<%=path%>/jumpDemo">模板管理</a></li>
		<li role="presentation" id="builder_query"  class="active"><a
			href="<%=path%>/builderQuery" >查询结构管理</a></li>
		<li role="presentation" id="query_result"><a
			href="<%=path%>/queryResult">查询结果管理</a></li>
	</ul>
	<div style="text-align: center;">
		<h2>QueryBuilder-jQuery 动态页面</h2>
	</div>
	<div class="mainDiv" id="sql_builder"></div>
	<div class="container-fluid"
		style="text-align: center;margin-top: 10px">
		<div class="btn-group btn-group-sm" role="group" aria-label="...">
			<button id="submit_btn" type="button" class="btn btn-primary">获取SQL结构</button>
			<button id="SQL_btn" type="button" class="btn btn-primary">解析SQL</button>
			<button type="button" class="btn btn-primary">重置</button>
		</div>
	</div>
	</div>

</body>
</html>