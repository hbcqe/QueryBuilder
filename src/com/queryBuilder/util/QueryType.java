package com.queryBuilder.util;

public class QueryType {

	/**
	 * 查询模板
	 */
	public static final String TEMPLATE = "templateData";

	/**
	 * 别名
	 */
	public static final String ALIAS = "aliasData";
	/**
	 * 结果显示列
	 */
	public static final String RESULTPROS = "resultPros";
	/**
	 * 所有的类
	 */
	public static final String ALLCLASSES = "getClasses";

	/**
	 * 添加模板
	 */
	public static final String ADDTEMPLATE = "addTemplate";

	/**
	 * 修改查询模板
	 */
	public static final String UPDATETEMPLATE = "updateTemplate";

	/**
	 * 修改查询模板名称
	 */
	public static final String EDITTEMPLATENAME = "editTemplateName";

	/**
	 * 编辑别名
	 */
	public static final String EDITALIANAME = "editAliaName";

	/**
	 * 新增别名
	 */
	public static final String ADDALIANAME = "addAliaName";

	/**
	 * 获取所有的别名属性
	 */
	public static final String GETALIAPRO = "getAliaPro";

	/**
	 * 保存要修改的别名
	 */
	public static final String UPDATEPROS = "updatePros";

	/**
	 * 删除数据
	 */
	public static final String DELETEDATA = "deleteData";

	
}
